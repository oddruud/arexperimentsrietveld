﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirehouseBoyController : MonoBehaviour
{

    public Animator BoyAnimator;
    public AudioSource BoyYelling;
    public float BoyNoticeDistance = 0.2f;

    private Camera cam;

    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
        Wave();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, cam.transform.position) < BoyNoticeDistance)
        {
            StandStill();
        }
        else
        {
            Wave();
        }
    }

    void Wave()
    {
        //BoyYelling.Play();
        BoyAnimator.SetBool("Wave", true);
    }

    void StandStill()
    {
        //BoyYelling.Pause();
        BoyAnimator.SetBool("Wave", false);
    }

}
