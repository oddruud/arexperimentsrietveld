﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugController : MonoBehaviour
{
    public bool EnableARMode;

    public Camera ARCamera;
    public Camera DebugCamera;

    // Use this for initialization
    void Start()
    {
        Vuforia.VuforiaBehaviour.Instance.enabled = EnableARMode;
        ARCamera.gameObject.SetActive(EnableARMode);
        DebugCamera.gameObject.SetActive(!EnableARMode);
        DebugCamera.clearFlags = CameraClearFlags.Skybox;
    }

    // Update is called once per frame
    void Update()
    {
        Vuforia.VuforiaBehaviour.Instance.enabled = EnableARMode;
    }
}
